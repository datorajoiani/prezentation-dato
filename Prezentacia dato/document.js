// Document Object თვისებებზე წვდომა

// დოკუმენტის სათაურის მიღება
const documentTitle = document.title;
console.log('Document Title:', documentTitle);

// დოკუმენტის სათაურის შეცვლა
document.title = 'New Document Title';

// ელემენტის მიღება მისი ID-ით
const elementById = document.getElementById('exampleId');
console.log('Element by ID:', elementById);

// ელემენტების მიღება კლასის სახელით
const elementsByClassName = document.getElementsByClassName('exampleClass');
console.log('Elements by Class Name:', elementsByClassName);

// ელემენტების მიღება tagname
const elementsByTagName = document.getElementsByTagName('p');
console.log('Elements by Tag Name:', elementsByTagName);

// ახალი ელემენტის შექმნა და დოკუმენტის სხეულში დამატება
const newElement = document.createElement('div');
newElement.textContent = 'This is a new element';
document.body.appendChild(newElement);
