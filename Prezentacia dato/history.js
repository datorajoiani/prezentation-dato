// History Object თვისებებზე წვდომა

// ბრაუზერის ისტორიის სიგრძის მიღება
const historyLength = window.history.length;
console.log('History Length:', historyLength);

// ბრაუზერის ისტორიაში დაბრუნება
function goBack() {
  window.history.back();
}

// წინ გადაადგილება ბრაუზერის ისტორიაში
function goForward() {
  window.history.forward();
}

// ისტორიაში კონკრეტულ URL-ზე ნავიგაცია (ამ შემთხვევაში, 2 ნაბიჯით უკან დაბრუნება)
function goBackBySteps(steps) {
  window.history.go(-steps);
}
