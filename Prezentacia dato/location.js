// მდებარეობის ობიექტის თვისებებზე წვდომა

// მიმდინარე URL-ის მიღება
 const currentURL = window.location.href;
console.log('Current URL:', currentURL);

// მიმდინარე URL-ის ჰოსტის სახელის მიღება
const hostName = window.location.hostname;
console.log('Hostname:', hostName);

// მიმდინარე URL-ის ბილიკის სახელის მიღება
const pathName = window.location.pathname;
console.log('Pathname:', pathName);


