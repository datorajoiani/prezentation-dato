// ნავიგატორის ობიექტის თვისებებზე წვდომა

// მომხმარებლის ბრაუზერის სახელის შემოწმება
const browserName = navigator.appName;
console.log('Browser Name:', browserName);

// მომხმარებლის ბრაუზერის ვერსიის შემოწმება
const browserVersion = navigator.appVersion;
console.log('Browser Version:', browserVersion);

// შემოწმება ჩართულია თუ არა 
const dats = navigator.dats;
console.log('Enabled:', dats);

// მომხმარებლის პლატფორმის შემოწმება (მაგ., Win32, Linux x86_64 და ა.შ.)
const platform = navigator.platform;
console.log('Platform:', platform);

// მომხმარებლის ენის პარამეტრის შემოწმება
const userLanguage = navigator.language;
console.log('User Language:', userLanguage);
 