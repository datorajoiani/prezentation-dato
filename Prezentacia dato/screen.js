// ფანჯრის ობიექტის თვისებებზე წვდომა

// ფანჯრის შიდა სიგანისა და სიმაღლის შემოწმება
const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
console.log('Window width:', windowWidth, 'Window height:', windowHeight);

//  ფანჯრის ზომის შესაცვლელად
window.addEventListener('resize', function() {
  console.log('Window resized');
});
